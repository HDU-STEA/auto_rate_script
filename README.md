# auto_rate_script

#### 介绍
一个自动化的杭电学评教脚本，需配合tamperMonkey使用

#### 安装教程
[点击下载安装本体](https://greasyfork.org/zh-CN/scripts/393610-%E6%9D%AD%E7%94%B5%E8%87%AA%E5%8A%A8%E8%AF%84%E4%BB%B7%E6%8F%92%E4%BB%B6)

[点击下载安装禁用alert](https://greasyfork.org/zh-CN/scripts/393611-%E7%A6%81%E7%94%A8alert)

#### 使用说明
需要同时开启本体和禁用alert，并在tamperMonkey设置中把配置模式选项设为“高级”，在页面下部找到“注入模式”并修改为“即时”